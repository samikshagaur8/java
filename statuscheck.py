import re

def getNumbers(s):
    MyNumRegex = re.search(r': (.*)/',s)
    if MyNumRegex is None:
        return None
    else:
        return (MyNumRegex.group(1))

array1 = []
var = 0
block = ""
found = False
with open("status1.txt","r") as origin_file:
    for line in origin_file.readlines():
        print(line)
        if found:
            block += line;
            if "Encountered" in line.strip(): break
        else:
            if line.strip() == "FM SUPERVISION":
                found = True
print(block)

for line in block.splitlines():
    array1.insert(var, getNumbers(line))
array1.reverse()

res = []

for val in array1: 
    if val != None :
        val1 = int(val)
        res.append(val1) 

print(res)
